#
# ~/.bash_profile
#

export QT_QPA_PLATFORMTHEME=qt5ct
export EDITOR="nvim"
export GTK_THEME=Adwaita:dark
export LESS='-N -R --use-color -Dd+r$Du+b'
export LS_COLORS='sg=30;'
export GTK_THEME_VARIANT=dark

[[ -f ~/.bashrc ]] && . ~/.bashrc

export PATH="$PATH:$HOME/.local/bin"

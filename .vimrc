let mapleader=','

" set title
set title

" theme
" set bg=dark

" highlight selection
set hlsearch

" ignore case while search
" set incsearch

" use system clipboard
set clipboard=unnamedplus

" auto indent
set autoindent

" tab to space
set expandtab

" file format
set fileformat=unix

" syntax hl
syntax on

" encoding of file
set encoding=utf-8

" relative numbers
set number relativenumber

" cmd tab complete
set wildmode=longest,list,full

" Highlight matching brace
set showmatch

" Use visual bell (no beeping)
set visualbell

" Enable smart-case search
set smartcase

" Always case-insensitive
set ignorecase

" Use 'C' style program indenting
set cindent

" ruler
set ruler

" split right
set splitbelow splitright

" global replace
nnoremap S :%s//g<left><right>

" paste from clipboard
map <S-Insert> <C-i>


" auto centre line
autocmd InsertEnter * norm zz

" delete spaces at end
autocmd BufWritePre * %s/\s\+$//e

" spell check
set spell!

" mouse support
set mouse=n

" set cursorline
" set cursorcolumn
" highlight CursorLine ctermbg=Yellow cterm=bold guibg=#2b2b2b
" highlight CursorColumn ctermbg=Yellow cterm=bold guibg=#2b2b2b



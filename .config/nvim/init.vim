let mapleader=' '

" set title
set title

" theme
set bg=dark

" true color
set termguicolors

" highlight selection
set hlsearch

" use system clipboard
set clipboard=unnamedplus

" auto indent
set autoindent

" file format
set fileformat=unix

" syntax hl
syntax on

" encoding of file
set encoding=utf-8

" relative numbers
set number number

" cmd tab complete
set wildmode=longest,list,full

" Highlight matching brace
set showmatch

" Use visual bell (no beeping)
set visualbell

" Enable smart-case search
set smartcase

" split right
set splitbelow splitright

" auto centre line
autocmd InsertEnter * norm zz

" delete spaces at end
autocmd BufWritePre * %s/\s\+$//e

" spell check
" set spell!

" mouse support
set mouse=a

" hides --INSERT--
set noshowmode

set cursorline
highlight CursorLine ctermbg=Yellow cterm=bold guibg=#2b2b2b

" split window
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" global replace
nnoremap S :%s///g<left><left><left>

" paste from clipboard
" map <C-p> <C-r><0>
map <C-a> ggVG
vmap <C-c> y<Esc>i
vmap <C-x> d<Esc>i
vmap <C-v> p<Esc>i
imap <C-v> <Esc>pi
imap <C-y> <Esc>ddi

imap <C-z> <Esc>ui
" map <C-c> <C->

call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-sensible'
Plug 'christianchiarulli/nvcode-color-schemes.vim'
Plug 'preservim/nerdtree'
Plug 'frazrepo/vim-rainbow'
Plug 'itchyny/lightline.vim'
Plug 'jiangmiao/auto-pairs'
call plug#end()

" nerdtree
let g:plug_window = 'noautocmd vertical topleft new'
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" always open nerdtree
" autocmd vimenter * NERDTree


" rainbow parenthesis
let g:rainbow_active = 1

" lightline
let g:lightline = {
      \ 'colorscheme': 'one',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'readonly', 'filepath', 'modified' ] ],
      \   'right': [ [ 'linei' ],
      \              [ 'percent' ],
      \              [ 'filetype' ] ]
      \ },
      \ 'component': {
      \   'filepath': '%f','linei': '%l/%L:%c'
      \ },
      \ }

" colorscheme
colorscheme onedark

" transparent background
hi Normal guibg=NONE ctermbg=NONE



#!/bin/bash

NUM="`cat ~/scripts/count.txt`"
LIST=$(find /share/Pictures/Wallpaper -type f)
TOTAL=$(echo "$LIST" | wc -l)

while true
do
	swaybg -i $(echo "$LIST" | sed "$((NUM+1))q;d" ) -m fill &

	RETURNSTAT=$?
	if [[ $RETURNSTAT != 0 ]]; then
		exit 0
	fi

	sleep 2
	if [[ !(-z $pid) ]]; then
		kill $pid
	fi
	pid=$!

	sleep 1800

	if [[ $((NUM+1)) == $TOTAL ]]; then
		NUM=0
		echo 0 > ~/scripts/count.txt
	else
		NUM=$((NUM+1))
		echo $NUM > ~/scripts/count.txt
	fi
done

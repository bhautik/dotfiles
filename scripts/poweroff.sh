swaynag -t normal -s Close -m "Select a Action" \
	-z 'Reboot' 'swaynag -t normal -s Close -m "Select a Action" -z 'Reboot' "loginctl reboot" ' \
	-z 'Poweroff' 'swaynag -t normal -s Close -m "Select a Action" -z 'Poweroff' "loginctl poweroff" ' \
	-z 'Suspend' 'loginctl suspend' \
	-z 'Exit' 'swaynag -t normal -s Close -m "Select a Action" -z 'Exit' "swaymsg exit" ' \
	-z 'Logout' 'swaynag -t normal -s Close -m "Select a Action" -z 'Logout' "exit" ' \
	-z 'Lock Session' '~/scripts/swaylock.sh'

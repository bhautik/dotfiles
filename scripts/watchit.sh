#!/bin/sh
mkdir -p $HOME/.cache/watchit
cachedir=$HOME/.cache/watchit

if [ -z $1 ]; then
  query=$(rofi -dmenu "Search Torrent: ")
else
  query=$1
fi

query="$(echo $query | sed 's/ /+/g')"
list=$(curl -s https://apibay.org/q.php?q=$query)

if [ ! -z "$(echo $list | grep "No results returned")" ]; then
	notify-send "No Results Found"
	exit 1
fi

echo $list | jq '.[].size | tonumber / 1048576 |round' | awk '{print NR " - ["$0" MB]"}' > $cachedir/size.txt
echo $list | jq -r '.[] | "\(.seeders) \(.leechers) \(.name)"' | awk '{printf "[S:"$1 ", L:"$2"]"; $1=$2=""; print ""$0"" }' > $cachedir/data.txt

LINE=$(paste -d\   $cachedir/size.txt $cachedir/data.txt | rofi -dmenu | awk NF=1)

if [ -z $LINE ]; then
  notify-send "No File Selected"
  exit 2
fi

LINE=$( expr $LINE - 1 )
infohash=$(echo $list | jq --argjson l $LINE -r '.[$l].info_hash')

# echo "1 Download \n 2 Stream" | rofi -dmenu
# choice=echo -e "1  Download \n2  Stream" | rofi -dmenu | awk NF=1

# if [ $choice -eq 1 ]; then
  qbittorrent --skip-dialog=true $infohash &
  notify-send "Downloading File"
# else
#   notify-send "Not implimented"
# fi

exit 0

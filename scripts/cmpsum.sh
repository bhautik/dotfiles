#!/bin/bash
# choose algorith
SUM=sha256sum

if [[ -z $* ]]; then
	echo "Please enter 2 values"
	exit 0
fi

SUM1=$("$SUM" "$1" | awk '{print $1;}')

if [[ $SUM1 = $2 ]]; then
	echo "checksum matched"
else
	echo "checksum is not same"
fi

#!/bin/bash

while true
do
	NUM="`cat ~/scripts/count.txt`"
	LIST=$(find /share/Pictures/Wallpaper -type f)
	TOTAL=$(echo "$LIST" | wc -l)
	NUM=$((NUM+1))

	if [[ $NUM -ge $TOTAL ]]; then
		echo 1 > ~/scripts/count.txt
	else
		echo $NUM > ~/scripts/count.txt
		echo $NUM >> ~/scripts/record.txt
	fi

	swaybg -i $(echo "$LIST" | sed "$((NUM-1))q;d" ) -m fill
	sleep 2000
done

#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <filesystem>
#include <dirent.h>
#include <unistd.h>
using namespace std;

int main()
{
  string line;
  int nLine = 0;
  fstream myfile, temp;
  DIR *dirp;
  struct dirent *dp;
  myfile.open("~/scripts/swaybg1.txt", ios::in | ios::out);
  if (myfile.is_open())
  {
    while (getline(myfile, line))
      ++nLine;
    cout << nLine;
    if (nLine < 100)
    {
      dirp = opendir("/share/Pictures/Wallpaper");
      readdir(dirp);
      readdir(dirp);
      cout << line << endl;
      myfile.seekp(0, ios::end);
      myfile.seekg(0, ios::end);
      myfile.clear();
      while ((dp = readdir(dirp)) != NULL)
      {
        myfile << "/share/Pictures/Wallpaper/" << dp->d_name << "\n";
      }
      (void)closedir(dirp);
    }
    myfile.close();
    while (true)
    {
      string s;
      temp.clear();
      myfile.clear();
      myfile.open("~/scripts/swaybg.txt", ios::in);
      temp.open("~/scripts/temp.txt", ios::out);
      getline(myfile, line);
      while (getline(myfile, s))
      {
        temp << s << "\n";
      }
      temp.close();
      myfile.close();
      remove("~/scripts/swaybg.txt");
      rename("~/scripts/temp.txt", "~/scripts/swaybg.txt");
      system(const_cast<char *>(("swaybg -m stretch -i" + line).c_str()));
      usleep(1800000);
      system(const_cast<char *>(("pkill swaybg.out")));
    }
  }
  else
    cout << "-1";
  return 0;
}
